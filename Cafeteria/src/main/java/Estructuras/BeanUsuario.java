package Estructuras;

public class BeanUsuario {

    private String correo;
    private String clave;
    private String nombre;
    private String rol;
    private String permiso;
    private String vista;
    private String css;
    private String modificador;
    private String etiqueta;
     
   public BeanUsuario(String correo,String clave,String nombre,String rol,String permiso,String vista,String css,String modificador,String etiqueta) {
        super();
        this.correo = correo;
        this.clave = clave;
        this.nombre = nombre;
        this.rol = rol;
        this.permiso = permiso;
        this.vista = vista;
        this.css = css;
        this.modificador = modificador;
        this.etiqueta=etiqueta;
    }
    
    /**
     * @return the correo
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * @param correo the correo to set
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * @return the clave
     */
    public String getClave() {
        return clave;
    }

    /**
     * @param clave the clave to set
     */
    public void setClave(String clave) {
        this.clave = clave;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the rol
     */
    public String getRol() {
        return rol;
    }

    /**
     * @param rol the rol to set
     */
    public void setRol(String rol) {
        this.rol = rol;
    }

    /**
     * @return the permiso
     */
    public String getPermiso() {
        return permiso;
    }

    /**
     * @param permiso the permiso to set
     */
    public void setPermiso(String permiso) {
        this.permiso = permiso;
    }

    /**
     * @return the vista
     */
    public String getVista() {
        return vista;
    }

    /**
     * @param vista the vista to set
     */
    public void setVista(String vista) {
        this.vista = vista;
    }

    /**
     * @return the css
     */
    public String getCss() {
        return css;
    }

    /**
     * @param css the css to set
     */
    public void setCss(String css) {
        this.css = css;
    }

    /**
     * @return the modificador
     */
    public String getModificador() {
        return modificador;
    }

    /**
     * @param modificador the modificador to set
     */
    public void setModificador(String modificador) {
        this.modificador = modificador;
    }

    /**
     * @return the etiqueta
     */
    public String getEtiqueta() {
        return etiqueta;
    }

    /**
     * @param etiqueta the etiqueta to set
     */
    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

}
