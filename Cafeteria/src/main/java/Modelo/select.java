package Modelo;

import Estructuras.BeanUsuario;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class select {
    public static BeanUsuario validarUsuario(String usuario, String clave) {
        BeanUsuario busuario = null;
        List<BeanUsuario> Etiqueta = new ArrayList<>();

        try {
            Conexion c = new Conexion();
            CallableStatement sp;
            ResultSet rs;

            Connection con = c.getConexion();
            sp = con.prepareCall("{ call SP_VALIDARUSUARIO(?,?) }");
            sp.setString("CORREO", usuario);
            sp.setString("CLAVE", clave);
            rs = sp.executeQuery(); 
            
            while (rs.next()) {
                busuario = new BeanUsuario(rs.getString("CORREO"), rs.getString("CLAVE"), rs.getString("NOMBRE"), rs.getString("ROL"), rs.getString("PERMISO"), rs.getString("VISTA"), rs.getString("CSS"), rs.getString("MODIFICADOR"), rs.getString("ETIQUETA"));
                Etiqueta.add(busuario);
                System.out.println("Bean: " + busuario.toString());
            }
        } catch (Exception e) {
        }
        return busuario;
    }    
}
