/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alpha
 */
public class select {
    public static BeanUsuario validarUsuario(String usuario, String clave) {
        BeanUsuario busuario = null;
        List<BeanUsuario> Etiqueta = new ArrayList<>();

        try {
            Conexion c = new Conexion();
            CallableStatement sp;
            ResultSet rs;

            Connection con = c.getConexion();
            sp = con.prepareCall("{ call SP_VALIDARUSUARIO(?,?) }");
            sp.setString("CORREO", usuario);
            sp.setString("CLAVE", clave);
            rs = sp.executeQuery(); 
            
            while (rs.next()) {
                busuario = new BeanUsuario(rs.getString("CORREO"), rs.getString("CLAVE"), rs.getString("NOMBRE"), rs.getString("ROL"), rs.getString("PERMISO"), rs.getString("VISTA"), rs.getString("CSS"), rs.getString("MODIFICADOR"), rs.getString("ETIQUETA"));
                Etiqueta.add(busuario);
                System.out.println("Nombre: " + busuario.getNombre());
                System.out.println("Rol: " + busuario.getRol());
                System.out.println("Bean: " + busuario.toString());
            }
        } catch (Exception e) {
        }
        return busuario;
    }    
    
}
