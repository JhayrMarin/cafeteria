<%@page import="Estructuras.BeanUsuario"%>
<%@page session="true" %>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.JsonPrimitive"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.FileNotFoundException"%>

<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.ParseException;"%>

<%@page import="Modelo.Conexion"%>
<%@page import="Modelo.select" %>

<%@page import="java.io.FileReader"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="Modelo.select"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="/webColab/css/MenuP.css" rel="stylesheet" type="text/css"/>
        <title>Menu Principal</title>
    </head>
    <body>

        <%
            String usuario = session.getAttribute("Usuario").toString();
            String nombre = session.getAttribute("Nombre").toString();
            String rol = session.getAttribute("Rol").toString();
        %>
        <nav style="background: #1D1D1F " class="navbar navbar-dark justify-content-between">
            <a style="color: white; " class="navbar-brand"></a>
            <div  class="btn-group">
                <button style="background: #1D1D1F; margin-right: .5cm" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <a> <img style="border-radius: 100px 100px 100px 100px" src="/webColab/Recursos/logo.jpg" height="30" width="30"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= nombre%>&nbsp;&nbsp;</a>
                </button>
                <div style="font-size: 12.5px;" class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item"class=""><%= nombre%></a> 
                    <a class="dropdown-item"><%= usuario%></a> 
                    <a class="dropdown-item"><%= rol%></a> 
                    <div  class="dropdown-divider"></div>
                    <a  class="dropdown-item"href="/webColab/sAutenticacion?btnLogin=Salir" class="">Salir</a>
                </div>
            </div>
        </nav>
        <a href="/webColab/sProducto" style="color: #0000;">Solicitud</a>

        <div class=" externo">
            <div class="interno">
                <table>
                    <tr><th>Menu de <%= rol%> </th></tr>
                            <%
                                String path = (select.class.getProtectionDomain().getCodeSource().getLocation().getPath()).replaceAll("/WEB-INF/classes/Modelo/select.class", "\\\\JSON\\\\menu.json");
                                String ruta2 = path.replace('/', '\\');
                                String ruta1 = ruta2.replaceAll("\\\\C:", "C:");
                                String ruta = ruta1.replace("%", " ");

                                JsonParser parser = new JsonParser();
                                FileReader fr = new FileReader(ruta);
                                JsonElement datos = parser.parse(fr);
                                System.out.println("info: " + datos.toString());

                                String jsonString = datos.toString();
                                System.out.println("json: " + jsonString);

                                Gson gson1 = new Gson();
                                List<BeanUsuario> listAlumnos = gson1.fromJson(jsonString, new TypeToken<List<BeanUsuario>>() {
                                }.getType());
                                if (listAlumnos != null) {
                                    for (BeanUsuario object : listAlumnos) {
                            %>
                    <tr><td><a href="<%=object.getVista() %>"><%=object.getEtiqueta()%></a></td></tr>
                            <%
                                    }
                                }
                            %>
                </table>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


    </body>
</html>
