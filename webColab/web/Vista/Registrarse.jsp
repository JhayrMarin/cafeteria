<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="/webColab/css/Registrar.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <title>Register</title>
    </head>

    <body>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-xl-9 mx-auto">
                    <div class="card card-signin flex-row my-5">
                        <div class="card-img-left d-none d-md-flex">
                            <!-- Background image for card set in CSS! -->
                        </div>
                        <div class="card-body">
                            <h5 class="card-title text-center">Registrarse</h5>

                            <form class="form-signin" action="/webColab/sRegistrarCliente" method="POST">
                                <div class="form-label-group">
                                    <input type="text" id="inputUserame" class="form-control"  name="txtNombre" placeholder="Username"
                                           required autofocus>
                                    <label for="inputUserame">Nombre</label>
                                </div>

                                <div class="form-label-group">
                                    <input type="email" id="inputEmail"  class="form-control" name="txtCorreo" placeholder="Email address"
                                           required>
                                    <label for="inputEmail">Correo</label>
                                </div>
                                <div class="form-label-group">
                                    <input type="text" id="inputEmail" class="form-control" name="txtNit" placeholder="Email address"
                                           required>
                                    <label for="inputEmail">NIT</label>
                                </div>
                                <div class="form-label-group">
                                    <input type="text" id="inputEmail" class="form-control" name="txtDireccion" placeholder="Email address"
                                           required>
                                    <label for="inputEmail">Dirección</label>
                                </div>

                                <hr>

                                <div class="form-label-group">
                                    <input type="password" id="inputPassword" name="txtClave1" class="form-control"
                                           placeholder="Password" required>
                                    <label for="inputPassword">Contraseña</label>
                                </div>



                                <input class="btn btn-lg btn-primary btn-block text-uppercase"type="submit" name="btnRegistrar" value="Registrar" class="btn login_btn">
                                <hr class="my-4">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</body>

</html>
