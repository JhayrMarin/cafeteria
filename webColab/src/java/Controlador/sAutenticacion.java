/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Estructuras.BeanUsuario;
import Modelo.select;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Alpha
 */
@WebServlet("/webColab/sAutenticacion")
public class sAutenticacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            if (request.getParameter("btnLogin").equals("Ingresar")) {
                String usuario = request.getParameter("txtUsuario");
                String clave = request.getParameter("txtClave");
                BeanUsuario busuario;
                busuario = select.validarUsuario(usuario, clave);

                HttpSession sesion = request.getSession();
                
                if (busuario != null) {
                    
                        sesion.setAttribute("Usuario", busuario.getCorreo());
                        sesion.setAttribute("Nombre", busuario.getNombre());
                        sesion.setAttribute("Rol", busuario.getRol());
                        request.getRequestDispatcher("Vista/MenuPrincipal.jsp").forward(request, response);
                    
                } else {
                    try (PrintWriter out = response.getWriter()) {
                        out.println("<script type=\"text/javascript\">");
                        out.println("alert('Usuario o contraseña inválido.');");
                        out.println("location='/webColab/Vista/Login.jsp';");
                        out.println("</script>");
                    }

                    request.getRequestDispatcher("Vista/Login.jsp").forward(request, response);
                }
            } else {
                request.getRequestDispatcher("Vista/Index.jsp").forward(request, response);
            }
        } catch (ServletException ex) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
