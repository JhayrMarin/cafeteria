/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Estructuras.BeanCliente;

import java.sql.*;

/**
 *
 * @author Alpha
 */
public class insert {
    public static BeanCliente InsertarCliente(String correo, String nombre, String nit, String direccion, String clave, int rol) {
        BeanCliente bcliente = null;
        try {
            Conexion c = new Conexion();
            CallableStatement sp;
            ResultSet rs;

            Connection con = c.getConexion();
            sp = con.prepareCall("{ call SP_INSERTARCLIENTE(?,?,?,?,?,?) }");
            sp.setString("CORREO", correo);
            sp.setString("NOMBRE", nombre);
            sp.setString("NIT", nit);
            sp.setString("DIRECCION", direccion);
            sp.setString("CLAVE", clave);
            sp.setInt("ROL", rol);
            rs = sp.executeQuery();

            con.close();
            rs.close();
            sp.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bcliente;
    }
    
}
