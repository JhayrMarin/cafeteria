/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Estructuras.BeanProducto;
import Estructuras.BeanUsuario;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alpha
 */
public class select {
     public static BeanUsuario validarUsuario(String usuario, String clave) {
        BeanUsuario busuario = null;
        List<BeanUsuario> Etiqueta = new ArrayList<>();

        try {
            Conexion c = new Conexion();
            CallableStatement sp;
            ResultSet rs;

            Connection con = c.getConexion();
            sp = con.prepareCall("{ call SP_VALIDARUSUARIO(?,?) }");
            sp.setString("CORREO", usuario);
            sp.setString("CLAVE", clave);
            rs = sp.executeQuery(); 
            
            while (rs.next()) {
                busuario = new BeanUsuario(rs.getString("CORREO"), rs.getString("CLAVE"), rs.getString("NOMBRE"), rs.getString("ROL"), rs.getString("PERMISO"), rs.getString("VISTA"), rs.getString("CSS"), rs.getString("MODIFICADOR"), rs.getString("ETIQUETA"));
                Etiqueta.add(busuario);             
                System.out.println("Bean: " + busuario.toString());
            }
            
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonEjemplo = gson.toJson(Etiqueta);
            System.out.println(jsonEjemplo);
//C:\Users\Alpha\Documents\NetBeansProjects\webColab\web\JSON
            String directorio = "C:\\Users\\Alpha\\Documents\\NetBeansProjects\\webColab\\web\\JSON\\menu.json";
            String path = (select.class.getProtectionDomain().getCodeSource().getLocation().getPath()).replaceAll("/WEB-INF/classes/Modelo/select.class", "\\\\JSON\\\\menu.json");
            String ruta2 = path.replace('/', '\\');
            String ruta1 = ruta2.replaceAll("\\\\C:", "C:");
            String ruta = ruta1.replace("%", " ");
            //String ruta=directorio;
            System.out.println(ruta);

            String contenido = jsonEjemplo;
            File file = new File(ruta);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(contenido);
            bw.close();

            JsonParser parser = new JsonParser();
            //Leo el JSON
            FileReader fr = new FileReader(ruta);
            JsonElement datos = parser.parse(fr);
            System.out.println("info: " + datos.toString());

            String jsonString = datos.toString();
            System.out.println("json: " + jsonString);

            Gson gson1 = new Gson();
            List<BeanUsuario> listAlumnos = gson1.fromJson(jsonString, new TypeToken<List<BeanUsuario>>() {
            }.getType());
            if (listAlumnos != null) {
                for (BeanUsuario object : listAlumnos) {
                    System.out.println("\nUsuario : " + object.getNombre() + " " + object.getEtiqueta()+ " " + object.getVista());
                }
            }

//            String destinatario = busuario.getUsuario(); //A quien le quieres escribir.
//            String asunto = "Correo de prueba enviado desde Java";
//            String cuerpo = "Esta es una prueba de correo...";
//            EnviarCorreo(destinatario, asunto, cuerpo);
            con.close();
            rs.close();
            sp.close();
            
        } catch (Exception e) {
        }
        return busuario;
    }    
     
      public static BeanProducto selectProducto() {
          BeanProducto bproducto = null;
        List<BeanProducto> Etiqueta = new ArrayList<>();

        try {
            Conexion c = new Conexion();
            CallableStatement sp;
            ResultSet rs;

            Connection con = c.getConexion();
            sp = con.prepareCall("{ call SP_SELECTPRODUCTO }");
            rs = sp.executeQuery(); 
            
            while (rs.next()) {
                bproducto = new BeanProducto(rs.getString("COD_PRODUCTO"),rs.getString("ID_CATEGORIA"),rs.getString("PRODUCTO"),rs.getFloat("PRECIO"),rs.getString("CLASE"),rs.getString("DESCRIPCION"),rs.getString("CATEGORIA"));
                Etiqueta.add(bproducto);             
                System.out.println("Bean: " + bproducto.toString());
            }
            
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String jsonEjemplo = gson.toJson(Etiqueta);
            System.out.println(jsonEjemplo);
//C:\Users\Alpha\Documents\NetBeansProjects\webColab\web\JSON
            String directorio = "C:\\Users\\Alpha\\Documents\\NetBeansProjects\\webColab\\web\\JSON\\producto.json";
            String path = (select.class.getProtectionDomain().getCodeSource().getLocation().getPath()).replaceAll("/WEB-INF/classes/Modelo/select.class", "\\\\JSON\\\\producto.json");
            String ruta2 = path.replace('/', '\\');
            String ruta1 = ruta2.replaceAll("\\\\C:", "C:");
            String ruta = ruta1.replace("%", " ");
            //String ruta=directorio;
            System.out.println(ruta);

            String contenido = jsonEjemplo;
            File file = new File(ruta);
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(contenido);
            bw.close();

            JsonParser parser = new JsonParser();
            //Leo el JSON
            FileReader fr = new FileReader(ruta);
            JsonElement datos = parser.parse(fr);
            System.out.println("info: " + datos.toString());

            String jsonString = datos.toString();
            System.out.println("json: " + jsonString);

            Gson gson1 = new Gson();
            List<BeanProducto> listAlumnos = gson1.fromJson(jsonString, new TypeToken<List<BeanProducto>>() {
            }.getType());
            if (listAlumnos != null) {
                for (BeanProducto object : listAlumnos) {
                    System.out.println("\nUsuario : " + object.getIdcategoria() + " " + object.getCategoria() +" " + object.getProducto()+" " + object.getPrecio() );
                }
            }

//            String destinatario = busuario.getUsuario(); //A quien le quieres escribir.
//            String asunto = "Correo de prueba enviado desde Java";
//            String cuerpo = "Esta es una prueba de correo...";
//            EnviarCorreo(destinatario, asunto, cuerpo);
            con.close();
            rs.close();
            sp.close();
            
        } catch (Exception e) {
        }
        return bproducto;
    }    

    
}
