/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras;

/**
 *
 * @author Alpha
 */
public class BeanCliente {

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    private String correo;
    private String nombre;
    private String nit;
    private String direccion;
    private String clave;
    private int rol;

    public BeanCliente(String correo, String nombre, String nit, String direccion, String clave, int rol) {
        super();
        this.correo = correo;
        this.nombre = nombre;
        this.nit = nit;
        this.direccion = direccion;
        this.clave = clave;
        this.rol = rol;
    }

  

}
