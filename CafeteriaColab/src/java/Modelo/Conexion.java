package Modelo;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    private Connection con = null;

    public Conexion() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            con = DriverManager.getConnection("jdbc:sqlserver://ALPHA-PC:1433;databaseName=Cafeteria", "sa", "1234");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            System.out.println(e.toString());
        }
    }

    public Connection getConexion() {
        return con;
    }
    
        public void cerrarConexion() {
        try {
            con.close();
        } catch (SQLException e) {
        }

    }
    
}
